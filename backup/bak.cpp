/*
 * GStreamer
 * Copyright (C) 2006 Stefan Kost <ensonic@users.sf.net>
 * Copyright (C) 2013  <<user@hostname.org>>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-constq
 *
 * FIXME:Describe constq here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 filesrc location=/Users/chain/Music/iTunes/iTunes\ Media/Music/Kamelot/Silverthorn\ \(CD1\)/03\ Ashes\ To\ Ashes.mp3  ! mpegaudioparse ! avdec_mp3 ! audioresample ! audioconvert ! constq ! videoconvert ! osxvideosink
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>
#include <gst/video/video.h>
#include <armadillo>

#include "gstconstq.h"

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x) // indirection magic :)

#define DEFAULT_WIDTH 100 //  300*44100/256
#define KERN_MAT_ROWS 841
//#define FFT_SIZE 2000
#define FFT_STEP 256  // samplerate/FFT_STEP = pixels per second  FIXME in the future make this definw a PROP so we can say that we want Y secs data into X pixels
#define HEIGHT_CAP ",height=(int)[ 1, " STR(KERN_MAT_ROWS) " ]"

// arma::fmat kernel(filter->height, FFT_SIZE/2+1);
// guitar lowest note E2 = 82Hz     (open low E string)
// guitar highest note E6 = 1,318Hz (24th fret on high E string)
//#include "cqt_kern.c"
//arma::fmat kernel(cqt_kern, KERN_MAT_ROWS, FFT_SIZE/2+1, false, true);


#define SRC_CAPS     GST_VIDEO_CAPS_MAKE("GRAY8") HEIGHT_CAP
#define SINK_CAPS    "audio/x-raw, format=(string)F32LE, channels=(int)1, rate=(int)44100" //sr is fixed because kernel depends on it. If different kernels for different samplerates are provided, it would be possible.

static GstStaticPadTemplate sink_template =
        GST_STATIC_PAD_TEMPLATE (
            "sink",
            GST_PAD_SINK,
            GST_PAD_ALWAYS,
            GST_STATIC_CAPS (SINK_CAPS)
            );

static GstStaticPadTemplate src_template =
        GST_STATIC_PAD_TEMPLATE (
            "src",
            GST_PAD_SRC,
            GST_PAD_ALWAYS,
            GST_STATIC_CAPS(SRC_CAPS)
            );

GST_DEBUG_CATEGORY_STATIC (gst_constq_debug);
#define GST_CAT_DEFAULT gst_constq_debug

/* Filter signals and args */
enum
{
    /* FILL ME */
    LAST_SIGNAL
};

enum
{
    PROP_0,
    PROP_SILENT,
    PROP_OFFSET, // from lowest bin, how many bins (maximum is kernel.n_rows-filter->height)
};



#define gst_constq_parent_class parent_class
G_DEFINE_TYPE (GstConstQ, gst_constq, GST_TYPE_ELEMENT);

static void gst_constq_set_property (GObject * object, guint prop_id,
                                     const GValue * value, GParamSpec * pspec);
static void gst_constq_get_property (GObject * object, guint prop_id,
                                     GValue * value, GParamSpec * pspec);

static GstFlowReturn
gst_constq_chain (GstPad * pad, GstObject * parent, GstBuffer * buffer);
static gboolean
gst_constq_sink_event (GstPad * pad, GstObject * parent, GstEvent * event);


/* initialize the constq's class */
static void
gst_constq_class_init (GstConstQClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;

    gobject_class = (GObjectClass *) klass;
    gstelement_class = (GstElementClass *) klass;

    gobject_class->set_property = gst_constq_set_property;
    gobject_class->get_property = gst_constq_get_property;

    g_object_class_install_property (gobject_class, PROP_SILENT,
                                     g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
                                                           FALSE, (GParamFlags)(G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE)));

    gst_element_class_set_details_simple (gstelement_class,
                                          "constq",
                                          "Generic/Filter",
                                          "Audio to constq spectrogram generator",
                                          "tonu@innotal.com");

    gst_element_class_add_pad_template (gstelement_class, gst_static_pad_template_get (&src_template));
    gst_element_class_add_pad_template (gstelement_class, gst_static_pad_template_get (&sink_template));

    GST_DEBUG_CATEGORY_INIT (gst_constq_debug, "constq", 0, "Audio to constq spectrogram");
}

/* initialize the new element
 * initialize instance structure
 */
static void
gst_constq_init (GstConstQ *filter)
{
    filter->silent = FALSE;
    filter->adapter = gst_adapter_new();

    filter->offset = 0;//from higher note down... 0 means from highest note-bin

    filter->sinkpad = gst_pad_new_from_static_template(&sink_template, "sink");
    gst_pad_set_chain_function(filter->sinkpad, GST_DEBUG_FUNCPTR(gst_constq_chain));
    gst_pad_set_event_function(filter->sinkpad, GST_DEBUG_FUNCPTR(gst_constq_sink_event));
    gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);

    filter->srcpad = gst_pad_new_from_static_template (&src_template, "src");

    gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad);

    for (int i=0;i<sizeof(filter->stft)/sizeof(filter->stft[0]);i++)
        filter->stft[i] = new STFT();


    filter->sp_kern[0].load("/Users/chain/codez/sl2/gst-pitchshift/src/m841_2048_adj2_A1toA8_10pxsemitone.bin", arma::arma_binary);
    filter->sp_kern[1].load("/Users/chain/codez/sl2/gst-pitchshift/src/m841_4096_adj2_A1toA8_10pxsemitone.bin", arma::arma_binary);
    filter->sp_kern[2].load("/Users/chain/codez/sl2/gst-pitchshift/src/m841_8192_adj2_A1toA8_10pxsemitone.bin", arma::arma_binary);
    filter->sp_kern[3].load("/Users/chain/codez/sl2/gst-pitchshift/src/m841_16384_adj2_A1toA8_10pxsemitone.bin", arma::arma_binary);

    filter->stft[0]->init(2048, FFT_STEP, STFT::Blackman);
    filter->stft[1]->init(4096, FFT_STEP, STFT::Blackman);
    filter->stft[2]->init(8192, FFT_STEP, STFT::Blackman);
    filter->stft[3]->init(16384, FFT_STEP, STFT::Blackman);

}

static void
gst_constq_set_property (GObject * object, guint prop_id,
                         const GValue * value, GParamSpec * pspec)
{
    GstConstQ *filter = GST_CONSTQ (object);

    switch (prop_id) {
    case PROP_SILENT:
        filter->silent = g_value_get_boolean (value);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
gst_constq_get_property (GObject * object, guint prop_id,
                         GValue * value, GParamSpec * pspec)
{
    GstConstQ *filter = GST_CONSTQ (object);

    switch (prop_id) {
    case PROP_SILENT:
        g_value_set_boolean (value, filter->silent);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}


static gboolean
gst_constq_sink_event (GstPad *pad, GstObject * parent, GstEvent * event)
{
    GstConstQ *filter = GST_CONSTQ(parent);
    gboolean ret = true;

    switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CAPS:
    {
        GstCaps *caps;

        gst_event_parse_caps (event, &caps);

        GstStructure *str_in = gst_caps_get_structure(caps,0);
        gst_structure_get_int(str_in, "rate", &(filter->rate));

        gst_event_unref (event);
        GST_DEBUG("Caps Event %" GST_PTR_FORMAT, caps);

        ret = true;

        break;
    }
    default:
        ret = gst_pad_push_event (filter->srcpad, event);
        break;
    }

    return ret;
}

static gboolean
gst_constq_src_setcaps (GstConstQ * filter, GstCaps * caps)
{
    GstStructure *structure;

    structure = gst_caps_get_structure (caps, 0);

    if (!gst_structure_get_int (structure, "width", &filter->width) ||
            !gst_structure_get_int (structure, "height", &filter->height)
            )
        goto error;


    return gst_pad_set_caps (filter->srcpad, caps);

    /* ERRORS */
error:
    {
        GST_DEBUG_OBJECT (filter, "error parsing caps");
        return FALSE;
    }
}

static gboolean
gst_constq_src_negotiate (GstConstQ * filter)
{
    GstCaps *othercaps, *target;
    GstStructure *structure;
    GstCaps *templ;
    GstQuery *query;
    GstBufferPool *pool = NULL;
    GstStructure *config;
    guint size, min, max;

    templ = gst_pad_get_pad_template_caps (filter->srcpad);

    GST_DEBUG_OBJECT (filter, "performing negotiation");
    GST_DEBUG("Src pad template caps %" GST_PTR_FORMAT, templ);

    /* see what the peer can do */
    othercaps = gst_pad_peer_query_caps(filter->srcpad, NULL);
    GST_DEBUG("Other pad caps %" GST_PTR_FORMAT, othercaps);

    if (othercaps) {
        target = gst_caps_intersect (othercaps, templ);
        gst_caps_unref (templ);
        gst_caps_unref (othercaps);

        if (gst_caps_is_empty (target))
            goto no_format;

        target = gst_caps_truncate (target);
    } else {
        target = gst_caps_copy (templ);
    }

    GST_DEBUG("target caps %" GST_PTR_FORMAT, target);

    structure = gst_caps_get_structure (target, 0);
    gst_structure_fixate_field_nearest_int (structure, "width", DEFAULT_WIDTH);
    gst_structure_fixate_field_nearest_int (structure, "height", KERN_MAT_ROWS);
    gst_structure_fixate_field_nearest_fraction (structure, "framerate", 1, 1);
    gst_constq_src_setcaps (filter, target);

    GST_DEBUG("set src caps (fixated) %" GST_PTR_FORMAT, target);

    /* find a pool for the negotiated caps now */
    query = gst_query_new_allocation (target, TRUE);

    if (!gst_pad_peer_query (filter->srcpad, query)) {
        /* no problem, we use the query defaults */
        GST_DEBUG_OBJECT (filter, "ALLOCATION query failed");
    }

    if (gst_query_get_n_allocation_pools (query) > 0) {
        /* we got configuration from our peer, parse them */
        gst_query_parse_nth_allocation_pool (query, 0, &pool, &size, &min, &max);
        GST_DEBUG("got pool from downstream %d %d %d", size, min, max);
    } else {
        GST_DEBUG("got no pool");
        pool = NULL;
        size = filter->width*filter->height;
        min = max = 0;
    }

    if (pool == NULL) {
        /* we did not get a pool, make one ourselves then */
        pool = gst_buffer_pool_new ();
    }

    config = gst_buffer_pool_get_config (pool);
    gst_buffer_pool_config_set_params (config, target, size, min, max);
    gst_buffer_pool_set_config (pool, config);


    if (filter->pool) {
        gst_buffer_pool_set_active (filter->pool, FALSE);
        gst_object_unref (filter->pool);
    }
    filter->pool = pool;

    /* and activate */
    gst_buffer_pool_set_active (pool, TRUE);

    gst_caps_unref (target);

    return TRUE;

no_format:
    {
        gst_caps_unref (target);
        return FALSE;
    }
}

/* make sure we are negotiated */
static GstFlowReturn
ensure_negotiated (GstConstQ * filter)
{
    gboolean reconfigure;

    reconfigure = gst_pad_check_reconfigure (filter->srcpad);

    /* we don't know an output format yet, pick one */
    if (reconfigure || !gst_pad_has_current_caps (filter->srcpad)) {
        if (!gst_constq_src_negotiate (filter))
            return GST_FLOW_NOT_NEGOTIATED;
    }
    return GST_FLOW_OK;
}


float goertzel_mag(int numSamples,int TARGET_FREQUENCY,int SAMPLING_RATE, float* data)
{
    int     k,i;
    float   floatnumSamples;
    float   omega,sine,cosine,coeff,q0,q1,q2,magnitude,real,imag;

    float   scalingFactor = numSamples / 2.0;

    floatnumSamples = (float) numSamples;
    k = (int) (0.5 + ((floatnumSamples * TARGET_FREQUENCY) / SAMPLING_RATE));
    omega = (2.0 * M_PI * k) / floatnumSamples;
    sine = sin(omega);
    cosine = cos(omega);
    coeff = 2.0 * cosine;
    q0=0;
    q1=0;
    q2=0;

    for(i=0; i<numSamples; i++)
    {
        q0 = coeff * q1 - q2 + data[i];
        q2 = q1;
        q1 = q0;
    }

    // calculate the real and imaginary results
    // scaling appropriately
    real = (q1 - q2 * cosine) / scalingFactor;
    imag = (q2 * sine) / scalingFactor;

    magnitude = sqrtf(real*real + imag*imag);
    return magnitude;
}

//make
arma::fmat proc( arma::fvec &in)
{
    arma::fmat m(841,100);



    return m;
}

static GstFlowReturn
gst_constq_chain (GstPad * pad, GstObject * parent, GstBuffer * buffer)
{
    GstConstQ *filter = GST_CONSTQ (parent);
    GstFlowReturn ret;

    if (GST_CLOCK_TIME_IS_VALID (GST_BUFFER_TIMESTAMP (buffer)))
        gst_object_sync_values (GST_OBJECT (filter), GST_BUFFER_TIMESTAMP (buffer));

    ret = ensure_negotiated (filter);
    if (ret != GST_FLOW_OK) {
        gst_buffer_unref (buffer);
        return ret;
    }


    /* don't try to combine samples from discont buffer */
    if (GST_BUFFER_FLAG_IS_SET (buffer, GST_BUFFER_FLAG_DISCONT)) {
        gst_adapter_clear (filter->adapter);
    }

    gst_adapter_push (filter->adapter, buffer);
    int inputCountSamples =  FFT_STEP*filter->width;// fixme
    if (gst_adapter_available(filter->adapter) >=  inputCountSamples*sizeof(float)) //FIXME OSamp
    {
        GstMapInfo outMapping;
        GstBuffer *outbuf = NULL;

        //get out buffer
        ret = gst_buffer_pool_acquire_buffer (filter->pool, &outbuf, NULL);
        gst_buffer_map (outbuf, &outMapping, GST_MAP_WRITE);
        guint8 *out = (guint8*)outMapping.data;

        //do process
        arma::fvec audioIn((float*)gst_adapter_map(filter->adapter, inputCountSamples*sizeof(float)), inputCountSamples, false, true);//FIXME OSamp


        arma::fmat magn = proc(audioIn); // stepsize const for all channels, fftsize changes. output fftsize_rows, 100 cols
        {
            // swap arma col-major to row-major for gstreamer
            float *in = magn.memptr();
            for (int y=0;y<magn.n_rows;y++)
            {
                for (int x=0;x<magn.n_cols;x++)
                {
                    float val = *(in + x*magn.n_rows + y) ; // transpose here.  armadillo mat is in col-major
                    *out++ = (val>255?255:val);// float to uint8. clip if needed..
                }
            }
        }
#if 0
        //do fft with different fftsizes
        arma::fmat magn1 = arma::abs(filter->stft[0]->forward(audioIn)); // stepsize const for all channels, fftsize changes. output fftsize_rows, 100 cols
        arma::fmat magn2 = arma::abs(filter->stft[1]->forward(audioIn));
        arma::fmat magn3 = arma::abs(filter->stft[2]->forward(audioIn));
        arma::fmat magn4 = arma::abs(filter->stft[3]->forward(audioIn));


        // convert to constant q
        arma::fmat cqt1 = (filter->sp_kern[0]) * (magn1);
        arma::fmat cqt2 = (filter->sp_kern[1]) * (magn2);
        arma::fmat cqt3 = (filter->sp_kern[2]) * (magn3);
        arma::fmat cqt4 = (filter->sp_kern[3]) * (magn4);

        arma::fmat cqt(filter->height, filter->width);
        int i;
        i=0;   cqt.submat((i*cqt.n_rows/4), 0, ((i+1)*cqt.n_rows/4) -1, cqt.n_cols-1) = cqt4.submat((i*cqt.n_rows/4), 0, ((i+1)*cqt.n_rows/4) -1, cqt.n_cols-1);
        i=1;   cqt.submat((i*cqt.n_rows/4), 0, ((i+1)*cqt.n_rows/4) -1, cqt.n_cols-1) = cqt3.submat((i*cqt.n_rows/4), 0, ((i+1)*cqt.n_rows/4) -1, cqt.n_cols-1) * 2;
        i=2;   cqt.submat((i*cqt.n_rows/4), 0, ((i+1)*cqt.n_rows/4) -1, cqt.n_cols-1) = cqt2.submat((i*cqt.n_rows/4), 0, ((i+1)*cqt.n_rows/4) -1, cqt.n_cols-1) * 4;
        i=3;   cqt.submat((i*cqt.n_rows/4), 0, ((i+1)*cqt.n_rows/4) -1, cqt.n_cols-1) = cqt1.submat((i*cqt.n_rows/4), 0, ((i+1)*cqt.n_rows/4) -1, cqt.n_cols-1) * 8;
        {
            // swap arma col-major to row-major for gstreamer
            float *in = cqt.memptr();
            for (int y=0;y<cqt.n_rows;y++)
            {
                for (int x=0;x<cqt.n_cols;x++)
                {
                    float val = *(in + x*cqt.n_rows + y) ; // transpose here.  armadillo mat is in col-major
                    *out++ = (val>255?255:val);// float to uint8. clip if needed..
                }
            }
        }
#endif
        gst_buffer_unmap (outbuf, &outMapping);
        gst_adapter_unmap(filter->adapter);
        gst_adapter_flush(filter->adapter,  inputCountSamples*sizeof(float));

        GST_BUFFER_TIMESTAMP (outbuf) =  GST_BUFFER_TIMESTAMP(buffer); // FIXME
   //     GST_BUFFER_DURATION (outbuf) =  GST_BUFFER_DURATION(buffer);// FIXME

        ret = gst_pad_push (filter->srcpad, outbuf);
    }

    return GST_FLOW_OK;
}

