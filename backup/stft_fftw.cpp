#include "stft.h"
#include <fftw3.h>
#include <math.h>

//Short Time Fourier Transform
// needed to generate spectrograms
STFT::STFT()
{
    fftwf_plan_with_nthreads(8);
}

void STFT::init(int sampleRate, int fftSize, int stepSize, WinType type)
{
    _sampleRate = sampleRate;
    _fftSize = fftSize;
    _stepSize = stepSize;

    _win.set_size(_fftSize);

    if  (type == Blackman)
    {
        for(int k=0; k < _fftSize; k++)
            _win[k] =  0.35875 - 0.48829*cos(2*M_PI*k/(_fftSize-1)) + 0.14128*cos(4*M_PI*k*(_fftSize-1)) - 0.01168*cos(6*M_PI*k*(_fftSize-1));
    }
    else if (type == Hann)
    {
        for(int k=0; k < _fftSize; k++)
            _win[k] = 0.5*(1-cos(2*M_PI*k/(_fftSize - 1)));
    }
    else if (type == Hamming)
    {
        for(int k=0; k < _fftSize; k++)
            _win[k] = (0.54 - 0.46*cos( 2* M_PI * k /(_fftSize-1)));
    }

    _storeFwd.set_size(_fftSize);
    _storeFwd.zeros();

    _storeInv.set_size(_fftSize);
    _storeInv.zeros();
}

STFT::~STFT()
{

}

arma::cx_fmat STFT::forward(const arma::fvec &input)
{
    int frameCount = (input.n_elem /_stepSize);
    fftwf_complex * fft_result  = ( fftwf_complex* ) fftwf_malloc( sizeof( fftwf_complex ) * frameCount * _fftSize );

    arma::fvec inBuf(input.n_elem + _fftSize); // all the samples plus for the next time
    inBuf.subvec(0, _fftSize-1) = _storeFwd;// restore eelmisest korrast
    inBuf.subvec(_fftSize, inBuf.n_elem-1) =  input;// ylej22nud nullida

    arma::fvec tmp(_fftSize);

    fftwf_plan plan_forward;
    plan_forward = fftwf_plan_dft_r2c_1d( _fftSize, NULL , NULL, FFTW_ESTIMATE );

    for (int i=0; i<frameCount; i++)
    {
        arma::fvec inputSamples( (float*)inBuf.memptr() + _stepSize + _stepSize*i, _fftSize, false, true);//one frame at a time using stepsize
        tmp = inputSamples % _win;

        fftwf_execute_dft_r2c( plan_forward,  tmp.memptr() , &fft_result[_fftSize*i] );
    }
    fftwf_destroy_plan(plan_forward);

    _storeFwd = inBuf.subvec( inBuf.n_elem - _fftSize,   inBuf.n_elem-1);

    return arma::cx_fmat((arma::cx_float*)fft_result, _fftSize, frameCount, false, true);
}

// siin inverse ajal peaks genereerima datat juurde
arma::fvec STFT::inverse(const arma::cx_fmat &cx)
{
    int frameCount = cx.n_cols;
    int countSamplesOut =  frameCount*_stepSize; //samples

    arma::fvec outputBuf(countSamplesOut + _fftSize); // all the samples plus for the next time
    outputBuf.subvec(0, _fftSize-1) = _storeInv;// restore eelmisest korrast
    outputBuf.subvec(_fftSize, outputBuf.n_elem - 1).zeros();// ylej22nud nullida

    fftwf_plan plan_inverse;
    plan_inverse = fftwf_plan_dft_c2r_1d(_fftSize, NULL, NULL, FFTW_ESTIMATE );

    arma::fvec tmp(_fftSize); // do ifft to tmp first
    for (int i=0; i<frameCount; i++)
    {
        fftwf_execute_dft_c2r( plan_inverse, (fftwf_complex*)cx.colptr(i), tmp.memptr() );

        // fftSize + frameCount*_stepSize
        int f = _stepSize + i*_stepSize; // first _fftSizeBytes are zero or from last invocation of this func
        int t = f + _fftSize-1;

        outputBuf.subvec(f, t) += (_win % tmp);
    }
    fftwf_destroy_plan(plan_inverse);

    // salvesta see j2rgmiseks korraks, returni ylej22nud
    _storeInv = outputBuf.subvec( outputBuf.n_elem - _fftSize,    outputBuf.n_elem-1);

    return outputBuf.subvec(0, outputBuf.n_elem - _fftSize-1);
}

arma::fvec STFT::binFreqs() const
{
    arma::fvec ret(_fftSize/2);

    for (int i=0;i<_fftSize/2;i++)
        ret[i] = i * _sampleRate / _fftSize;

    // i = hz /(sr/fftsz)
    return ret;
}
