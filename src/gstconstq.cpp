/*
 * GStreamer
 * Copyright (C) 2006 Stefan Kost <ensonic@users.sf.net>
 * Copyright (C) 2013  <<user@hostname.org>>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-constq
 *
 * FIXME:Describe constq here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 filesrc location=/Users/chain/Music/iTunes/iTunes\ Media/Music/Kamelot/Silverthorn\ \(CD1\)/03\ Ashes\ To\ Ashes.mp3  ! mpegaudioparse ! avdec_mp3 ! audioresample ! audioconvert ! constq ! videoconvert ! osxvideosink
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

//#define MULTIRES_RESO_FACTOR 1.2
#define MULTIRES_RESO_FACTOR 1.1

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>
#include <gst/video/video.h>
#include <string.h>
#include <armadillo>

#include "gstconstq.h"

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x) // indirection magic :)

#define DEFAULT_WIDTH 380 //  300*44100/256
#define KERN_MAT_ROWS 384 // 48*8octaves

// arma::fmat kernel(filter->height, FFT_SIZE/2+1);
// guitar lowest note E2 = 82Hz     (open low E string)
// guitar highest note E6 = 1,318Hz (24th fret on high E string)
//#include "cqt_kern.c"
//arma::fmat kernel(cqt_kern, KERN_MAT_ROWS, FFT_SIZE/2+1, false, true);

static int fft_sizes[] = {
    600,
    1000,
    1600,
    2592,
    4320,
    6912,
    11250,
    18000,
};
#define SAMPLERATE 44100

#define MY_VIDEO_CAPS_MAKE(format)                                     \
    "video/x-raw, "                                                     \
    "format = (string) " format ", "                                    \
    "width = " GST_VIDEO_SIZE_RANGE ", "                                \
    "height=(int)[ 1, " STR(KERN_MAT_ROWS) " ]," \
    "framerate = " GST_VIDEO_FPS_RANGE

#define SRC_CAPS     MY_VIDEO_CAPS_MAKE("GRAY8")
#define SINK_CAPS    "audio/x-raw, format=(string)F32LE, channels=(int)1, rate=(int)" STR(SAMPLERATE) //sr is fixed because kernel depends on it. If different kernels for different samplerates are provided, it would be possible.

static GstStaticPadTemplate sink_template =
        GST_STATIC_PAD_TEMPLATE (
            "sink",
            GST_PAD_SINK,
            GST_PAD_ALWAYS,
            GST_STATIC_CAPS (SINK_CAPS)
            );

static GstStaticPadTemplate src_template =
        GST_STATIC_PAD_TEMPLATE (
            "src",
            GST_PAD_SRC,
            GST_PAD_ALWAYS,
            GST_STATIC_CAPS(SRC_CAPS)
            );

GST_DEBUG_CATEGORY_STATIC (gst_constq_debug);
#define GST_CAT_DEFAULT gst_constq_debug

/* Filter signals and args */
enum
{
    /* FILL ME */
    LAST_SIGNAL
};

enum
{
    PROP_0,
    PROP_SILENT,
    PROP_PXPERSEC // from lowest bin, how many bins (maximum is kernel.n_rows-filter->height)
};



#define gst_constq_parent_class parent_class
G_DEFINE_TYPE (GstConstQ, gst_constq, GST_TYPE_ELEMENT);

static void gst_constq_set_property (GObject * object, guint prop_id,
                                     const GValue * value, GParamSpec * pspec);
static void gst_constq_get_property (GObject * object, guint prop_id,
                                     GValue * value, GParamSpec * pspec);

static GstFlowReturn
gst_constq_chain (GstPad * pad, GstObject * parent, GstBuffer * buffer);
static gboolean
gst_constq_sink_event (GstPad * pad, GstObject * parent, GstEvent * event);

static
void gst_constq_do_transform(GstConstQ *filter, const guint8 *in, guint8 *out, int numSamples);
static GstFlowReturn
gst_constq_map_transform_push(GstConstQ *filter);


/* initialize the constq's class */
static void
gst_constq_class_init (GstConstQClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;

    gobject_class = (GObjectClass *) klass;
    gstelement_class = (GstElementClass *) klass;

    gobject_class->set_property = gst_constq_set_property;
    gobject_class->get_property = gst_constq_get_property;

    g_object_class_install_property (gobject_class, PROP_SILENT,
                                     g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
                                                           FALSE, (GParamFlags)(G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE)));
    g_object_class_install_property (gobject_class, PROP_PXPERSEC,
                                     g_param_spec_float ("px_per_sec", "Pixels per second", "Used for zooming",
                                                         0, 1000, 100,
                                                         (GParamFlags)(G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE)));

    gst_element_class_set_details_simple (gstelement_class,
                                          "constq",
                                          "Generic/Filter",
                                          "Audio to constq spectrogram generator",
                                          "tonu@innotal.com");

    gst_element_class_add_pad_template (gstelement_class, gst_static_pad_template_get (&src_template));
    gst_element_class_add_pad_template (gstelement_class, gst_static_pad_template_get (&sink_template));

    GST_DEBUG_CATEGORY_INIT (gst_constq_debug, "constq", 0, "Audio to constq spectrogram");
}

/* initialize the new element
 * initialize instance structure
 */
static void
gst_constq_init (GstConstQ *filter)
{
    filter->silent = FALSE;
    filter->adapter = gst_adapter_new();

    filter->step_size = 1024;

    filter->sinkpad = gst_pad_new_from_static_template(&sink_template, "sink");
    gst_pad_set_chain_function(filter->sinkpad, GST_DEBUG_FUNCPTR(gst_constq_chain));
    gst_pad_set_event_function(filter->sinkpad, GST_DEBUG_FUNCPTR(gst_constq_sink_event));
    gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);

    filter->srcpad = gst_pad_new_from_static_template (&src_template, "src");

    gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad);

    for (int i=0;i<sizeof(filter->stft)/sizeof(filter->stft[0]);i++)
        filter->stft[i] = new STFT();

    filter->harmonics.load("h.bin", arma::arma_binary);

    filter->sp_kern[0].load("m1.bin", arma::arma_binary);// highest freq, smallest fft
    filter->sp_kern[1].load("m2.bin", arma::arma_binary);
    filter->sp_kern[2].load("m3.bin", arma::arma_binary);
    filter->sp_kern[3].load("m4.bin", arma::arma_binary);
    filter->sp_kern[4].load("m5.bin", arma::arma_binary);
    filter->sp_kern[5].load("m6.bin", arma::arma_binary);
    filter->sp_kern[6].load("m7.bin", arma::arma_binary);
    filter->sp_kern[7].load("m8.bin", arma::arma_binary);

    for (int i=0;i<8;i++)
        filter->sp_kern[i] /= fft_sizes[i] / (254.0 * 13.0);//gain

    double multistep = filter->step_size;
    for (int i=0;i<8;i++)
    {
        filter->stft[i]->init(fft_sizes[i], multistep, STFT::Blackman);
        multistep *= MULTIRES_RESO_FACTOR;
    }
}

static void
gst_constq_set_property (GObject * object, guint prop_id,
                         const GValue * value, GParamSpec * pspec)
{
    GstConstQ *filter = GST_CONSTQ (object);

    switch (prop_id) {
    case PROP_SILENT:
        filter->silent = g_value_get_boolean (value);
        break;
    case PROP_PXPERSEC:
    {
        filter->step_size = SAMPLERATE / g_value_get_float(value);

        // fancy shit... need shader to extract from this
        // in st texture coords
        double multistep = filter->step_size;
        for (int i=0;i<8;i++)
        {
            filter->stft[i]->init(fft_sizes[i], multistep, STFT::Blackman);
            multistep *= MULTIRES_RESO_FACTOR;
        }
    }
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
gst_constq_get_property (GObject * object, guint prop_id,
                         GValue * value, GParamSpec * pspec)
{
    GstConstQ *filter = GST_CONSTQ (object);

    switch (prop_id) {
    case PROP_SILENT:
        g_value_set_boolean (value, filter->silent);
        break;
    case PROP_PXPERSEC:
        g_value_set_float(value, SAMPLERATE / filter->step_size);

        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}


static gboolean
gst_constq_sink_event (GstPad *pad, GstObject * parent, GstEvent * event)
{
    GstConstQ *filter = GST_CONSTQ(parent);
    gboolean ret = true;

    switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CAPS:

        gst_event_unref (event);

        break;
    case GST_EVENT_SEGMENT:
    {
        gst_adapter_clear (filter->adapter);

        GST_DEBUG("GOT SEEK!");
        ret = gst_pad_event_default (pad, parent, event);
        break;
    }
    case GST_EVENT_EOS:
    {
        fprintf(stderr, "got1 eos\n");

        // pad adapter with zeros
        int bufferSize =  sizeof(float) * filter->step_size *filter->width;
        gsize bytesMissing = bufferSize - gst_adapter_available(filter->adapter);
        if (bytesMissing>0)
        {
            GstMapInfo outMapping;
            GstBuffer * zeroedBuffer =  gst_buffer_new_allocate(NULL, bytesMissing, NULL);
            gst_buffer_map (zeroedBuffer, &outMapping, GST_MAP_WRITE);
            memset(outMapping.data, 0, bytesMissing);
            gst_buffer_unmap (zeroedBuffer, &outMapping);

            gst_adapter_push(filter->adapter, zeroedBuffer);
            //FIXME unref extra?
        }

        gst_constq_map_transform_push(filter);

        ret = gst_pad_event_default (pad, parent, event);
    }
    default:
        ret = gst_pad_event_default (pad, parent, event);
        break;
    }

    return ret;
}

static gboolean
gst_constq_src_setcaps (GstConstQ * filter, GstCaps * caps)
{
    GstStructure *structure;

    structure = gst_caps_get_structure (caps, 0);

    if (!gst_structure_get_int (structure, "width", &filter->width) || !gst_structure_get_int (structure, "height", &filter->height))
        goto error;

    //clear current buffer
    gst_adapter_clear (filter->adapter);

    return gst_pad_set_caps (filter->srcpad, caps);

    /* ERRORS */
error:
    {
        GST_DEBUG_OBJECT (filter, "error parsing caps");
        return FALSE;
    }
}

static gboolean
gst_constq_src_negotiate (GstConstQ * filter)
{
    GstCaps *othercaps, *target;
    GstStructure *structure;
    GstCaps *templ;
    GstQuery *query;
    GstBufferPool *pool = NULL;
    GstStructure *config;
    guint size, min, max;

    templ = gst_pad_get_pad_template_caps (filter->srcpad);

    GST_DEBUG_OBJECT (filter, "performing negotiation");
    GST_DEBUG("Src pad template caps %" GST_PTR_FORMAT, templ);

    /* see what the peer can do */
    othercaps = gst_pad_peer_query_caps(filter->srcpad, NULL);
    GST_DEBUG("Other pad caps %" GST_PTR_FORMAT, othercaps);

    if (othercaps) {
        target = gst_caps_intersect (othercaps, templ);
        gst_caps_unref (templ);
        gst_caps_unref (othercaps);

        if (gst_caps_is_empty (target))
            goto no_format;

        target = gst_caps_truncate (target);
    } else {
        target = gst_caps_copy (templ);
    }

    GST_DEBUG("target caps %" GST_PTR_FORMAT, target);

    structure = gst_caps_get_structure (target, 0);

    gint w;
    gint h;
    if (gst_structure_get_int(structure, "width", &w) == true)
        gst_structure_fixate_field_nearest_int (structure, "width", w);
    else
        gst_structure_fixate_field_nearest_int (structure, "width", DEFAULT_WIDTH);


    // when sink is 1px high, output summed magnitude
    filter->sum = false;
    if (gst_structure_get_int(structure, "height", &h) == true)
        if (h == 1)
            filter->sum = true;
    if (filter->sum == true)
        gst_structure_fixate_field_nearest_int (structure, "height", 1);
    else
        gst_structure_fixate_field_nearest_int (structure, "height", KERN_MAT_ROWS);

    gst_structure_fixate_field_nearest_fraction (structure, "framerate", 1, 1);
    gst_constq_src_setcaps (filter, target);

    GST_DEBUG("set src caps (fixated) %" GST_PTR_FORMAT, target);

    /* find a pool for the negotiated caps now */
    query = gst_query_new_allocation (target, TRUE);

    if (!gst_pad_peer_query (filter->srcpad, query)) {
        /* no problem, we use the query defaults */
        GST_DEBUG_OBJECT (filter, "ALLOCATION query failed");
    }

    if (gst_query_get_n_allocation_pools (query) > 0) {
        /* we got configuration from our peer, parse them */
        gst_query_parse_nth_allocation_pool (query, 0, &pool, &size, &min, &max);
        GST_DEBUG("got pool from downstream %d %d %d", size, min, max);
    } else {
        pool = NULL;
        size = filter->width*filter->height;
        min = max = 0;
        GST_DEBUG("got no pool. creating for size %d", size);
    }

    if (pool == NULL) {
        /* we did not get a pool, make one ourselves then */
        pool = gst_buffer_pool_new ();
    }

    config = gst_buffer_pool_get_config (pool);
    gst_buffer_pool_config_set_params (config, target, size, min, max);
    gst_buffer_pool_set_config (pool, config);


    if (filter->pool) {
        gst_buffer_pool_set_active (filter->pool, FALSE);
        gst_object_unref (filter->pool);
    }
    filter->pool = pool;

    /* and activate */
    gst_buffer_pool_set_active (pool, TRUE);

    gst_caps_unref (target);


    return TRUE;

no_format:
    {
        gst_caps_unref (target);
        return FALSE;
    }
}

/* make sure we are negotiated */
static GstFlowReturn
ensure_negotiated (GstConstQ * filter)
{
    gboolean reconfigure;

    reconfigure = gst_pad_check_reconfigure (filter->srcpad);

    /* we don't know an output format yet, pick one */
    if (reconfigure || !gst_pad_has_current_caps (filter->srcpad)) {
        if (!gst_constq_src_negotiate (filter))
        {
            return GST_FLOW_NOT_NEGOTIATED;
        }
    }
    return GST_FLOW_OK;
}


static GstFlowReturn
gst_constq_chain (GstPad * pad, GstObject * parent, GstBuffer * buffer)
{

    GstConstQ *filter = GST_CONSTQ (parent);

    if (GST_CLOCK_TIME_IS_VALID (GST_BUFFER_TIMESTAMP (buffer)))
        gst_object_sync_values (GST_OBJECT (filter), GST_BUFFER_TIMESTAMP (buffer));


    GstFlowReturn ret;
    ret = ensure_negotiated (filter);
    if (ret != GST_FLOW_OK) {
        gst_buffer_unref (buffer);
        return ret;
    }


    /* don't try to combine samples from discont buffer */
    if (GST_BUFFER_FLAG_IS_SET (buffer, GST_BUFFER_FLAG_DISCONT)) {
        gst_adapter_clear (filter->adapter);
    }

    gst_adapter_push (filter->adapter, buffer);

    return gst_constq_map_transform_push(filter);
}


static GstFlowReturn
gst_constq_map_transform_push(GstConstQ *filter)
{
    int inputCountSamples =  filter->step_size *filter->width;// fixme
    if (gst_adapter_available(filter->adapter) >=  inputCountSamples*sizeof(float)) //FIXME OSamp
    {

        GstMapInfo outMapping;
        GstBuffer *outbuf = NULL;

        //get out buffer
        gst_buffer_pool_acquire_buffer (filter->pool, &outbuf, NULL);
        gst_buffer_map (outbuf, &outMapping, GST_MAP_WRITE);

        gst_constq_do_transform(filter,
                                (guint8*)gst_adapter_map(filter->adapter, inputCountSamples*sizeof(float)),
                                (guint8*)outMapping.data,
                                inputCountSamples);

        gst_adapter_unmap(filter->adapter);
        gst_adapter_flush(filter->adapter,  inputCountSamples*sizeof(float));

        gst_buffer_unmap (outbuf, &outMapping);

        //????     GST_BUFFER_TIMESTAMP (outbuf) =  GST_BUFFER_TIMESTAMP(buffer); // FIXME
        GST_BUFFER_DURATION (outbuf) = filter->width / (SAMPLERATE/filter->step_size) * GST_SECOND;// FIXME

        return gst_pad_push (filter->srcpad, outbuf);
    }
    return GST_FLOW_OK;
}

static void
gst_constq_do_transform(GstConstQ *filter, const guint8 *in, guint8 *out, int numSamples)
{
    //do process
    arma::fvec audioIn((float*)in, numSamples, false, true);//FIXME OSamp
    {
        //FIXME: matrix mult to GPU or something
        arma::fmat cqt(filter->height, filter->width);

        int i;
        int subHeight =   filter->height / 8;
        i=0; cqt.submat((i*subHeight), 0, ((i+1)*subHeight) -1, (numSamples / filter->stft[7]->stepSize()) -1) = filter->sp_kern[7] * arma::abs(filter->stft[7]->forward(audioIn));
        i=1; cqt.submat((i*subHeight), 0, ((i+1)*subHeight) -1, (numSamples / filter->stft[6]->stepSize()) -1) = filter->sp_kern[6] * arma::abs(filter->stft[6]->forward(audioIn));
        i=2; cqt.submat((i*subHeight), 0, ((i+1)*subHeight) -1, (numSamples / filter->stft[5]->stepSize()) -1) = filter->sp_kern[5] * arma::abs(filter->stft[5]->forward(audioIn));
        i=3; cqt.submat((i*subHeight), 0, ((i+1)*subHeight) -1, (numSamples / filter->stft[4]->stepSize()) -1) = filter->sp_kern[4] * arma::abs(filter->stft[4]->forward(audioIn));
        i=4; cqt.submat((i*subHeight), 0, ((i+1)*subHeight) -1, (numSamples / filter->stft[3]->stepSize()) -1) = filter->sp_kern[3] * arma::abs(filter->stft[3]->forward(audioIn));
        i=5; cqt.submat((i*subHeight), 0, ((i+1)*subHeight) -1, (numSamples / filter->stft[2]->stepSize()) -1) = filter->sp_kern[2] * arma::abs(filter->stft[2]->forward(audioIn));
        i=6; cqt.submat((i*subHeight), 0, ((i+1)*subHeight) -1, (numSamples / filter->stft[1]->stepSize()) -1) = filter->sp_kern[1] * arma::abs(filter->stft[1]->forward(audioIn));
        i=7; cqt.submat((i*subHeight), 0, ((i+1)*subHeight) -1, (numSamples / filter->stft[0]->stepSize()) -1) = filter->sp_kern[0] * arma::abs(filter->stft[0]->forward(audioIn));

        // swap arma col-major to row-major for gstreamer
        float *in = cqt.memptr();

        for (int y=0;y<cqt.n_rows;y++)
        {
            for (int x=0;x<cqt.n_cols;x++)
            {
                float v = *(in + x*cqt.n_rows + y);

                *out++ = (v > 255.0? 255.0: v); // transpose here.  armadillo mat is in col-major
            }
        }
    }
}
