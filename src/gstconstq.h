/* 
 * GStreamer
 * Copyright (C) 2006 Stefan Kost <ensonic@users.sf.net>
 * Copyright (C) 2013  <<user@hostname.org>>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __GST_CONSTQ_H__
#define __GST_CONSTQ_H__

#include <armadillo>
#include <gst/gst.h>
#include "stft.h"

G_BEGIN_DECLS

#define GST_TYPE_CONSTQ \
  (gst_constq_get_type())
#define GST_CONSTQ(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_CONSTQ,GstConstQ))
#define GST_CONSTQ_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_CONSTQ,GstConstQClass))
#define GST_IS_CONSTQ(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_CONSTQ))
#define GST_IS_CONSTQ_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_CONSTQ))

typedef struct _GstConstQ      GstConstQ;
typedef struct _GstConstQClass GstConstQClass;

struct _GstConstQ {
  GstElement element;

  gboolean silent;

  gint width;  // aka FFT frames
  gint height; // aka FFT bins
  gint sum; // when height is 1px, sum whole fft frame
  gint step_size; // how many samples to step when doing FFT


  GstBufferPool *pool;
  GstAdapter *adapter;

  GstPad *sinkpad;
  GstPad *srcpad;

  STFT *stft[8]; // multires. 2000 8000 16000

  arma::SpMat<float> sp_kern[8];


  arma::SpMat<float> harmonics;
};

struct _GstConstQClass {
  GstElementClass parent_class;
};

GType gst_constq_get_type (void);

G_END_DECLS

#endif /* __GST_CONSTQ_H__ */
