/**
 * SECTION:element-plugin
 *
 * FIXME:Describe plugin here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! plugin ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>

#include "gstpitchshift.h"

GST_DEBUG_CATEGORY_STATIC (gst_pitchshift_debug);
#define GST_CAT_DEFAULT gst_pitchshift_debug

#define CHUNK_COUNT 10
#define CHUNK_SZ      (CHUNK_COUNT * FFT_STEP_SIZE * filter->channels * sizeof(float))

//GST_DEBUG=3 gst-launch-1.0 filesrc location=/Users/chain/Movies/Knife\ Party\ -\ \ Internet\ Friends\ .mp3 ! mpegaudioparse ! avdec_mp3 ! audioresample ! audioconvert ! pitchshift ! audioconvert ! portaudiosink

/* Filter signals and args */
enum
{
    /* FILL ME */
    LAST_SIGNAL
};

enum
{
    PROP_0,
    PROP_PITCH,
    PROP_AV_SYNC,
    PROP_SILENT
};

/* the capabilities of the inputs and outputs.
 *
 * FIXME:describe the real formats here.
 */

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE ("sink",
                                                                     GST_PAD_SINK,
                                                                     GST_PAD_ALWAYS,
                                                                     GST_STATIC_CAPS ( "audio/x-raw, format=(string)F32LE, layout=(string)interleaved" )
                                                                     );

static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE ("src",
                                                                    GST_PAD_SRC,
                                                                    GST_PAD_ALWAYS,
                                                                    GST_STATIC_CAPS ( "audio/x-raw, format=(string)F32LE, layout=(string)interleaved" )
                                                                    );


#define gst_pitchshift_parent_class parent_class
G_DEFINE_TYPE (GstPitchShift, gst_pitchshift, GST_TYPE_BASE_TRANSFORM);

static void gst_pitchshift_set_property (GObject * object, guint prop_id,
                                         const GValue * value, GParamSpec * pspec);
static void gst_pitchshift_get_property (GObject * object, guint prop_id,
                                         GValue * value, GParamSpec * pspec);

static GstCaps* gst_pitchshift_transform_caps(GstBaseTransform *trans,
                                              GstPadDirection direction,
                                              GstCaps *caps, GstCaps *filter);

static gboolean      gst_pitchshift_transform_size (GstBaseTransform *trans,
                                                    GstPadDirection direction,
                                                    GstCaps *caps, gsize size,
                                                    GstCaps *othercaps, gsize *othersize);

static GstFlowReturn gst_pitchshift_transform(GstBaseTransform *trans, GstBuffer *inbuf,  GstBuffer *outbuf);
static gboolean gst_pitchshift_set_caps(GstBaseTransform *trans, GstCaps *incaps, GstCaps *outcaps);
static gboolean gst_pitchshift_start(GstBaseTransform *trans);
static gboolean gst_pitchshift_sink_event (GstBaseTransform * trans, GstEvent * event);

static gboolean gst_pitchshift_query (GstBaseTransform * trans, GstPadDirection direction, GstQuery * query);

/* GObject vmethod implementations */

/* initialize the plugin's class */
static void
gst_pitchshift_class_init (GstPitchShiftClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;

    gobject_class = (GObjectClass *) klass;
    gstelement_class = (GstElementClass *) klass;

    gobject_class->set_property = gst_pitchshift_set_property;
    gobject_class->get_property = gst_pitchshift_get_property;

    g_object_class_install_property (gobject_class, PROP_PITCH,
                                     g_param_spec_float ("pitch", "Pitch", "1.0 normal, 2.0 octave higher.",
                                                         0.5, 2.0, 1.0,
                                                         (GParamFlags)(G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE)));

    g_object_class_install_property (gobject_class, PROP_AV_SYNC,
                                     g_param_spec_float ("avsync", "AV sync", "audio video syncing value in (s)",
                                                         -5, 5, 0.0,
                                                         (GParamFlags)(G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE)));
    g_object_class_install_property (gobject_class, PROP_SILENT,
                                     g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
                                                           FALSE,
                                                           (GParamFlags)(G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE)));

    gst_element_class_set_details_simple (gstelement_class,
                                          "PitchShift",
                                          "Generic/Filter",
                                          "PitchShift Filter",
                                          "tonu@innotal.com");

    gst_element_class_add_pad_template (gstelement_class,  gst_static_pad_template_get (&src_template));
    gst_element_class_add_pad_template (gstelement_class,  gst_static_pad_template_get (&sink_template));

    GST_BASE_TRANSFORM_CLASS (klass)->start = GST_DEBUG_FUNCPTR (gst_pitchshift_start);
    GST_BASE_TRANSFORM_CLASS (klass)->transform_caps = GST_DEBUG_FUNCPTR (gst_pitchshift_transform_caps);
    GST_BASE_TRANSFORM_CLASS (klass)->transform_size = GST_DEBUG_FUNCPTR (gst_pitchshift_transform_size);
    GST_BASE_TRANSFORM_CLASS (klass)->transform = GST_DEBUG_FUNCPTR (gst_pitchshift_transform);
    GST_BASE_TRANSFORM_CLASS (klass)->set_caps = GST_DEBUG_FUNCPTR (gst_pitchshift_set_caps);
    GST_BASE_TRANSFORM_CLASS (klass)->sink_event = GST_DEBUG_FUNCPTR (gst_pitchshift_sink_event);
    GST_BASE_TRANSFORM_CLASS (klass)->query = GST_DEBUG_FUNCPTR (gst_pitchshift_query);


    /* debug category for fltering log messages
   *
   * FIXME:exchange the string 'Template plugin' with your description
   */
    GST_DEBUG_CATEGORY_INIT (gst_pitchshift_debug, "pitch", 0, "PitchShift plugin");
}

/* initialize the new element
 * initialize instance structure
 */
static void
gst_pitchshift_init (GstPitchShift *filter)
{
    filter->shifter = new PitchShift();
    filter->silent = FALSE;

    filter->rate = 1.0;
    filter->pitch = 1.0;

    filter->latency =  0;
    filter->sampleRate = 1;
    filter->channels = 2;
    filter->need_setup = true;

    filter->avsync = 0;
    filter->adapterIn = gst_adapter_new();
}

gboolean gst_pitchshift_start(GstBaseTransform *trans)
{
    GstPitchShift *pitch_shift = GST_PITCHSHIFT (trans);

    gst_adapter_clear(pitch_shift->adapterIn);

    return true;
}

gboolean gst_pitchshift_set_caps(GstBaseTransform *trans, GstCaps *incaps, GstCaps *outcaps)
{
    GstPitchShift *pitch_shift = GST_PITCHSHIFT (trans);

    GstStructure *str = gst_caps_get_structure(incaps,0);

    gst_structure_get_int(str, "rate", &(pitch_shift->sampleRate));
    gst_structure_get_int(str, "channels", &(pitch_shift->channels));

    //setup with new params
    pitch_shift->shifter->init(pitch_shift->channels);
    pitch_shift->need_setup=true;

    return TRUE;
}



GstCaps* gst_pitchshift_transform_caps(GstBaseTransform *trans,
                                       GstPadDirection direction,
                                       GstCaps *caps, GstCaps *filter)
{
    gst_caps_ref(caps);
    return caps;
}

gboolean      gst_pitchshift_transform_size (GstBaseTransform *trans,
                                             GstPadDirection direction,
                                             GstCaps *caps, gsize size,
                                             GstCaps *othercaps, gsize *othersize)
{
    GstPitchShift *filter = GST_PITCHSHIFT (trans);

    if (filter->need_setup == true)
    {
        filter->need_setup = false;
        filter->shifter->setup(filter->pitch, filter->rate,  filter->sampleRate);
    }

    if (direction == GST_PAD_SRC)
    {
        *othersize = filter->shifter->inputSize(size);
    }
    else if (GST_PAD_SINK)
    {
        if ((size+gst_adapter_available (filter->adapterIn)) >= CHUNK_SZ)
            *othersize = filter->shifter->outputSize(CHUNK_SZ);
        else
            *othersize = 0;
    }

    return true;
}

/* this function does the actual processing
 */
static GstFlowReturn
gst_pitchshift_transform (GstBaseTransform * base, GstBuffer *inbuf, GstBuffer *outbuf)
{
    GstPitchShift *filter = GST_PITCHSHIFT (base);
    if (GST_CLOCK_TIME_IS_VALID (GST_BUFFER_TIMESTAMP (outbuf)))
        gst_object_sync_values (GST_OBJECT (filter), GST_BUFFER_TIMESTAMP (outbuf));

    gst_buffer_ref(inbuf);
    gst_adapter_push (filter->adapterIn, inbuf);

    GstMapInfo outMapping;
    gst_buffer_map (outbuf, &outMapping, GST_MAP_WRITE);
    if (gst_adapter_available (filter->adapterIn) >= CHUNK_SZ)
    {
        float *adapterData = (float*)gst_adapter_map (filter->adapterIn, CHUNK_SZ);

        filter->shifter->process(adapterData, (float*)outMapping.data, CHUNK_COUNT);

        gst_adapter_unmap (filter->adapterIn);
        gst_adapter_flush (filter->adapterIn, CHUNK_SZ);
    }
    gst_buffer_unmap (outbuf, &outMapping);

    GstClockTime timestamp;
    timestamp = GST_BUFFER_TIMESTAMP(inbuf) - filter->segment_start;
    if (timestamp < filter->latency)
        timestamp = 0;
    else
        timestamp -= filter->latency ;

    timestamp -= filter->avsync; // video hilineb, v2henda numbrit
    GST_BUFFER_TIMESTAMP(outbuf) = timestamp / filter->rate + filter->segment_start;
    GST_BUFFER_DURATION(outbuf) =  gst_util_uint64_scale (gst_buffer_get_size(outbuf), GST_SECOND,  (sizeof(float)*filter->channels) * filter->sampleRate) / filter->rate;

    return GST_FLOW_OK;
}

static void
gst_pitchshift_set_property (GObject * object, guint prop_id,
                             const GValue * value, GParamSpec * pspec)
{
    GstPitchShift *pitch_shift = GST_PITCHSHIFT (object);

    switch (prop_id) {
    case PROP_PITCH:
        pitch_shift->pitch = g_value_get_float (value);
        pitch_shift->need_setup = true;

        break;
    case PROP_SILENT:
        pitch_shift->silent = g_value_get_boolean (value);
        break;
    case PROP_AV_SYNC:
        pitch_shift->avsync = g_value_get_float(value) * GST_SECOND;
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
gst_pitchshift_get_property (GObject * object, guint prop_id,
                             GValue * value, GParamSpec * pspec)
{
    GstPitchShift *filter = GST_PITCHSHIFT (object);

    switch (prop_id) {
    case PROP_PITCH:
        g_value_set_float(value, filter->pitch);
        break;
    case PROP_SILENT:
        g_value_set_boolean (value, filter->silent);
        break;
    case PROP_AV_SYNC:
        g_value_set_float(value, filter->avsync/(double)GST_SECOND);

        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}



static gboolean
gst_pitchshift_sink_event (GstBaseTransform * trans, GstEvent * event)
{
    if (GST_EVENT_TYPE(event) == GST_EVENT_SEGMENT)
    {
        GstPitchShift *filter = GST_PITCHSHIFT (trans);
        GstSegment segment;

        gst_event_copy_segment (event, &segment);

        //        if (filter->rate != segment.rate) {
        //            filter->rate = segment.rate;
        //            filter->need_setup=true;
        //        }
        filter->shifter->setup(filter->pitch, segment.rate,  filter->sampleRate);
        filter->rate = filter->shifter->realRate();
        printf("rate: %f real_rate: %f\n", segment.rate, filter->rate);


        filter->segment_start = segment.start;
        segment.applied_rate = filter->rate;
        segment.rate = 1.0;
        gst_event_unref (event);

        if (segment.stop != -1) {
            segment.stop = (segment.stop - segment.start) / segment.applied_rate + segment.start;
        }

        event = gst_event_new_segment (&segment);
        gst_pad_push_event (GST_BASE_TRANSFORM_SRC_PAD (trans), event);

        gint64 bytesPerFrame =  (sizeof(float)*filter->channels);//fixme
        GstClockTime latency = gst_util_uint64_scale (CHUNK_SZ, GST_SECOND, bytesPerFrame * filter->sampleRate);
        if (filter->latency != latency)
        {
            filter->latency = latency;
            gst_element_post_message (GST_ELEMENT (filter), gst_message_new_latency (GST_OBJECT (filter)));
        }

        // clear buffer
        gst_adapter_clear(filter->adapterIn);

        return TRUE;
    }
    return GST_BASE_TRANSFORM_CLASS (parent_class)->sink_event (trans, event);
}



static gboolean
gst_pitchshift_query (GstBaseTransform * trans, GstPadDirection direction,
                      GstQuery * query)
{
    GstPitchShift *filter = GST_PITCHSHIFT (trans);

    if (direction == GST_PAD_SRC) {
        switch (GST_QUERY_TYPE (query)) {
        case GST_QUERY_LATENCY:{
            GstPad *peer;
            gboolean res;

            if ((peer = gst_pad_get_peer (GST_BASE_TRANSFORM_SINK_PAD (trans)))) {
                if ((res = gst_pad_query (peer, query))) {
                    GstClockTime min, max;
                    gboolean live;

                    gst_query_parse_latency (query, &live, &min, &max);

                    min += filter->latency;
                    if (max != GST_CLOCK_TIME_NONE)
                        max += filter->latency;

                    gst_query_set_latency (query, live, min, max);
                }
                gst_object_unref (peer);
            }

            return TRUE;
            break;
        }
        default:{
            return GST_BASE_TRANSFORM_CLASS (parent_class)->query (trans, direction,
                                                                   query);
            break;
        }
        }
    } else {
        return GST_BASE_TRANSFORM_CLASS (parent_class)->query (trans, direction,
                                                               query);
    }
}
