#ifndef __GST_PITCHSHIFT_H__
#define __GST_PITCHSHIFT_H__

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>
#include <gst/base/gstadapter.h>

#include <pitchshift.h>

G_BEGIN_DECLS

#define GST_TYPE_PITCHSHIFT \
    (gst_pitchshift_get_type())
#define GST_PITCHSHIFT(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_PITCHSHIFT,GstPitchShift))
#define GST_PITCHSHIFT_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_PITCHSHIFT,GstPitchShiftClass))
#define GST_IS_PITCHSHIFT(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_PITCHSHIFT))
#define GST_IS_PITCHSHIFT_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_PITCHSHIFT))

typedef struct _GstPitchShift      GstPitchShift;
typedef struct _GstPitchShiftClass GstPitchShiftClass;

struct _GstPitchShift {
    GstBaseTransform element;

    GstPad *srcPad;
    GstPad *sinkPad;

    gint sampleRate;
    gint channels;
    gint64 segment_start;
    GstClockTime latency;


    gboolean silent;
    gboolean need_setup;

    gfloat rate;
    gfloat pitch;
    PitchShift *shifter;
    GstAdapter *adapterIn;

    gint64 avsync;
};

struct _GstPitchShiftClass {
    GstBaseTransformClass parent_class;
};

GType gst_pitchshift_get_type (void);

G_END_DECLS

#endif /* __GST_PITCHSHIFT_H__ */
