#include "pitch.h"

#ifdef    ARMA_64BIT_WORD
#error cant use arma::uvec
#endif

Pitch::Pitch()
{
    //confitav
}


void Pitch::init(float fftSize, float stepSize, float frames)
{
    xxx= 0;
    _stepSize = stepSize;

    // float constants
    _fftSize = fftSize;
    _fftAnaSize = fftSize/2+1;
    _framesIn = frames;
    _framesOut = frames;
    _osamp = _fftSize/_stepSize;

    // input
    _magn.set_size(_fftAnaSize, frames); // calc magn
    _phase.set_size(_fftAnaSize, frames);

    // output
    _synthFreqPhase.set_size(_fftAnaSize, _framesOut);
    _synthMagn.set_size(_fftAnaSize, _framesOut);
    _out.set_size(_fftSize, _framesOut);

    // storage
    _lastPhase.set_size(_fftAnaSize);
    _lastPhase.zeros();
    _lastSynPhases.set_size(_fftAnaSize);
    _lastSynPhases.zeros();

    // precalculated vector constants
    float expct = 2.*M_PI*_stepSize/_fftSize;
    _expectedPhaseDiff.set_size(_fftAnaSize);
    for (float k=0;k<_expectedPhaseDiff.n_elem; k++)
        _expectedPhaseDiff[k] = k*expct;

    _binFreqs.set_size(_fftAnaSize);
    _binFreqsOut.set_size(_fftAnaSize);

    // temporaries
    _qpd.set_size(_fftAnaSize, frames);
    _phaseFreq.set_size(_fftAnaSize, frames);
}

void Pitch::setup(float pitch, float samplerate, int fftSizeOut,  int fftStepSizeOut) // runtime stuff (moved here from init to make reinit with new samplerate faster)
{
    _pitch = pitch;
    _sampleRate = samplerate;
    _fftSizeOut = fftSizeOut;
    _stepSizeOut = fftStepSizeOut;

    // for input
    _freqPerBin = _sampleRate/_fftSize;
    for (float k=0;k<_binFreqs.n_elem; k++)
        _binFreqs[k] = k*_freqPerBin;

    _optim3 = _freqPerBin*_osamp/(2.0*M_PI);

    // for output
    _freqPerBinOut = _sampleRate/_fftSizeOut;
    for (float k=0;k<_binFreqsOut.n_elem; k++)
        _binFreqsOut[k] = k*_freqPerBinOut;

    _optim1 = 2.0*M_PI / (_sampleRate/_stepSizeOut); //optimization
    _optim2 = _expectedPhaseDiff -  _binFreqsOut * _optim1 ;
}

arma::cx_fmat & Pitch::shift(const arma::cx_fmat &complex)//cols=frames, rows= fftbins and count of rows is fftSize/2+1
{
    //initialize vectors & matrices to new sizes if changed since last time
    if (_framesIn != complex.n_cols)
        init( 2*(complex.n_rows-1), _stepSize, complex.n_cols);

    ////* ***************** ANALYSIS ******************* */
    /* compute magnitude */
    _magn = arma::abs(complex);

    /* compute phase */
    arma::cx_fmat::const_iterator cb = complex.begin();
    arma::cx_fmat::const_iterator ce = complex.end();
    arma::fmat::iterator pb = _phase.begin();
    arma::fmat::iterator j=pb;

    for(arma::cx_fmat::const_iterator i=cb; i!=ce; ++j, ++i)
        *j = arg(*i);

    //    for (int i=0;i<_magn.n_rows;i++)
    //        printf("%d, magn:%f phase:%f\n", i, _magn(i,0), _phase(i,0));

    /* compute phase difference between conseqtive frames */
    // go from left to right
    _phaseFreq.col(0) = _phase.col(0) - _lastPhase;
    for (int i=1;i<_framesIn; i++)
        _phaseFreq.col(i) = _phase.col(i) - _phase.col(i-1);
    _lastPhase = _phase.col(_framesIn-1);// store last one for next time

    /* subtract expected phase difference */
    _phaseFreq.each_col() -= _expectedPhaseDiff;

    /* map delta phase into +/- Pi interval */
    _qpd = arma::conv_to<arma::imat>::from(_phaseFreq / M_PI);
    _qpd.transform( [](arma::sword val) -> arma::sword { if (val >= 0) return val + ((val)&1); else return val - ((val)&1); } );
    _phaseFreq -= M_PI * arma::conv_to<arma::fmat>::from(_qpd);

    /* get deviation from bin frequency from the +/- Pi interval */
    /* compute the true frequencies */
    _phaseFreq *= _optim3;
    _phaseFreq.each_col() += _binFreqs;

    /* ***************** PROCESSING ******************* */
    /* this does the actual pitch shifting */
    _synthMagn.zeros(); //change the output size here
      _synthFreqPhase.zeros();


    // faasi peab akumuleerima mitte iga bini kohta vaid iga center freq kohta (iga sinusoidi). neid on v2hem kui bine
#if 0
    for (int i=0;i<_fftAnaSize;i++)
    {
        int shiftedBinNr = i*_pitch;
        if(shiftedBinNr < _fftAnaSize)
        {
            for (int frame = 0; frame<_phaseFreq.n_cols; frame++)
            {
                float partialsNewFreqency = _phaseFreq(shiftedBinNr,frame) * _pitch;
                int fixedBinNr = freqToBin(partialsNewFreqency);

                _synthMagn(fixedBinNr, frame) += _magn(i, frame);
                _synthFreqPhase(fixedBinNr, frame) = partialsNewFreqency;
            }
        }
    }
    //   _synthFreqPhase /= _synthMagn;
#else
    if (_pitch != 1.0)
    {
        for (int i=0;i<_fftAnaSize;i++)
        {
            int shiftedBinNr = i*_pitch;
            if(shiftedBinNr < _fftAnaSize)
            {
                _synthMagn.row(shiftedBinNr) += _magn.row(i);
                _synthFreqPhase.row(shiftedBinNr) = _phaseFreq.row(i) * _pitch;
            }
        }
    }
    else
    {
        _synthMagn = _magn;
        _synthFreqPhase = _phaseFreq;
    }
#endif
    //   _synthFreqPhase = _phaseFreq;

    //        for (int i=0;i<_synthFreqPhase.n_rows;i++)
    //            printf("bin:%d> binfreq:%f magn:%f freq:%f  origMagn:%f  origFreq:%f\n", i, (i * _sampleRate / _fftSize),_synthMagn(i,0), _synthFreqPhase(i, 0) ,  _magn(i,0), _phaseFreq(i,0));

    /* ***************** SYNTHESIS ******************* */
    /* subtract bin mid frequency */
    /* get bin deviation from freq deviation */
    /* take osamp into account */
    /* add the overlap phase advance back in */
    _synthFreqPhase *= _optim1;
    _synthFreqPhase.each_col() += _optim2;

    _synthFreqPhase.col(0) += _lastSynPhases;
    for (int i=1;i<_framesOut; i++)
        _synthFreqPhase.col(i) += _synthFreqPhase.col(i-1);
    _lastSynPhases = _synthFreqPhase.col(_synthFreqPhase.n_cols-1);

    _out = arma::cx_fmat(arma::cos(_synthFreqPhase), arma::sin(_synthFreqPhase)) % _synthMagn;

    return _out;
}

inline int clamp(int x, int a, int b)
{
    return x < a ? a : (x > b ? b : x);
}
int Pitch::freqToBin(float frequency) const
{
    //freq = binNr * _sampleRate / _fftSize;
    return clamp(round(frequency / ((float)_sampleRate / _fftSize)), 0, _fftAnaSize-1);
}
