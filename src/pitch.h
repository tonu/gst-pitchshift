#ifndef PITCH_H
#define PITCH_H

#include <armadillo>


class Pitch
{
public:
    Pitch();

    // in-place.
    // rows->bins from freq 1 till nyquist (not incl) ???
    // cols->batch of ffts
    arma::cx_fmat &shift(const arma::cx_fmat &complex);

    void init(float fftSize, float stepSize, float frames);
    void setup(float pitch, float samplerate, int fftSizeOut,  int fftStepSizeOut);

    void setPitch(float pitch);
    int freqToBin(float frequency) const;

private:
    float _pitch; // multiplier 1.0->none; 2.0->octave higher...

    arma::fvec _lastPhase;
    arma::fvec _expectedPhaseDiff;
    arma::fvec _expectedPhaseDiffOut;
    arma::fvec _binFreqs;
    arma::fvec _binFreqsOut;
    arma::fvec _lastSynPhases;
    arma::imat _qpd;
    arma::cx_fmat _out;
    arma::fmat _phaseFreq;
    arma::fmat _magn;
    arma::fmat _phase;
    arma::fmat _synthFreqPhase;
    arma::fmat _synthMagn;


    float _sampleRate;
    float _fftSize;
    float _fftSizeOut;
    float _fftAnaSize;
    float _stepSize;
    float _stepSizeOut;
    float _freqPerBin;
    float _freqPerBinOut;
    float _osamp;
    float _framesIn;
    float _framesOut;

    float _optim1;
    float _optim3;
    arma::fvec _optim2;

    float xxx;
};

#endif // PITCH_H
