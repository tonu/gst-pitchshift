#include "pitchshift.h"

#include <pitch.h>
#include <stft.h>
#include <gst/fft/fft.h>


// TODO.. all floats that can be, should be converted to ints ... note that int/int might produce errors!

inline float clamp(float x, float a, float b)
{
    return x < a ? a : (x > b ? b : x);
}

PitchShift::PitchShift()
{

    _channels = 0;
    _fftSize = (FFT_SIZE);
    _stepSize = FFT_STEP_SIZE;
}

float PitchShift::inputUnitSize() const // in bytes
{
    return _stepSize  * _channels * sizeof(float);
}
float PitchShift::outputUnitSize() const
{
    return _stepSizeOut * _channels * sizeof(float);
}

int PitchShift::outputSize(int input) const
{
    //printf("in:%d out:%f\n", input, (float)input * (_fftSizeOut/FFT_SIZE));
    return round((float)input * (_fftSizeOut/FFT_SIZE));
}

int PitchShift::inputSize(int output) const
{
   // printf("out:%d in:%f\n", output, (float)output / (_fftSizeOut/FFT_SIZE));
    return round((float)output / (_fftSizeOut/FFT_SIZE));
}

float PitchShift::realRate() const
{
    return _rate;
}

void PitchShift::init(int channels)
{
    if (channels != _channels) {
        _channels = channels;

        for(std::vector<STFT*>::iterator it = _stfters.begin(); it != _stfters.end(); ++it)
            delete *it;
        _stfters.clear();

        for (int i=0;i<_channels; i++)
        {
            STFT *stft = new STFT();
            stft->init(_fftSize, _stepSize, STFT::Hann);
            _stfters.push_back(stft);
        }

        for(std::vector<Pitch*>::iterator it = _pitchShifters.begin(); it != _pitchShifters.end(); ++it)
            delete *it;
        _pitchShifters.clear();

        for (int i=0;i<_channels; i++)
        {
            Pitch *shifter = new Pitch();
            shifter->init(_fftSize, _stepSize, 1);
            _pitchShifters.push_back(shifter);
        }
    }
}

void PitchShift::setup(float pitch, float rate, int samplerate)
{
    //fixme.. make rate<1.0 slower
    rate = 1.0/rate;

    _pitch = clamp(pitch, 0.5, 2.0);
    rate = clamp(rate, 0.5, 5.0);

    _stepSizeOut = rate * _stepSize;
    int i=0;
    while (true)
    {
        _fftSizeOut =  gst_fft_next_fast_length((_stepSizeOut+i) * FFT_OSAMP);
        //printf("trying to find next best stepsize: %d  %f==%f\n", i, _fftSizeOut / (FFT_SIZE/FFT_STEP_SIZE), round(_fftSizeOut / (FFT_SIZE/FFT_STEP_SIZE)));
        if (_fftSizeOut / FFT_OSAMP == round(_fftSizeOut / FFT_OSAMP))
        {
            _stepSizeOut = _fftSizeOut / FFT_OSAMP;// _stepsizeout depends on gst_fft_next_fast_length
            break;
        }
        i++;
    }
    _rate = _fftSize/(float)_fftSizeOut;// and also _rate depends. So actual _rate depends on gst_fft_next_fast_length

    //printf("==== %f %f %f   %f->%f; %f->%f\n", rate, _rate, pitch, _fftSize, _fftSizeOut, _stepSize, _stepSizeOut);
    for (int i=0;i<_channels; i++)
    {
        _pitchShifters[i]->setup(pitch,  samplerate, _fftSizeOut, _stepSizeOut);
        _stfters[i]->setup(_fftSizeOut, _stepSizeOut, STFT::Hann);
    }
}

//FIXME (invert) outputi jaoks peaks olema eraldi fft size
void PitchShift::process(float *in, float *out, int n_fftFrames) // WORK_SIZE bytes input
{
    // each column represents a frame. each row channel
    arma::fmat input(in,   _channels,   n_fftFrames *   inputUnitSize() / (_channels*sizeof(float)), false, true);
    arma::fmat output(out, _channels,   n_fftFrames *  outputUnitSize() / (_channels*sizeof(float)), false, true);

    if (_rate == 1.0 && _pitch == 1.0)
    {
        output = input;
        return;
    }
    //printf("proc: n_fftFrames:%d   %f %f\n",n_fftFrames,  n_fftFrames *   inputUnitSize() / (_channels*sizeof(float)), n_fftFrames *  outputUnitSize() / (_channels*sizeof(float)));
    //#pragma omp parallel for
    for (int chan=0; chan<_channels; chan++)
    {
        //        // do forward STFT &
        //        // shift in freq domain &
        //        // inverse STFT
        output.row(chan) = trans(_stfters[chan]->inverse(_pitchShifters[chan]->shift(_stfters[chan]->forward(trans(input.row(chan))))));
        //        output.row(chan) = trans(_stfters[chan]->inverse(_stfters[chan]->forward(trans(input.row(chan)))));
    }
    output /= _fftSize * FFT_OSAMP * 0.26; // FIXME..
}
