#ifndef PITCHSHIFT_H
#define PITCHSHIFT_H

#include <armadillo>
#include <gst/fft/fft.h>

//https://ccrma.stanford.edu/~jos/sasp/COLA_Examples.html
#define FFT_SIZE ((float)gst_fft_next_fast_length(3*600))
#define FFT_OSAMP 3.0// teoorias sobiks siia 2, aga probleemid nagu vertical coherence ja canceling out ...
#define FFT_STEP_SIZE  (FFT_SIZE / FFT_OSAMP) // google Blackman Harris COLA

// NB!

class Pitch;
class STFT;

class PitchShift
{
public:
    PitchShift();

    void init(int channels);
    void setup(float pitch, float rate, int samplerate);
    void process(float *in, float *out, int n_fftFrames);

    float realRate() const;
    float inputUnitSize() const;
    float outputUnitSize() const;
    int outputSize(int input) const;
    int inputSize(int output) const;

private:
    std::vector<STFT*> _stfters;
    std::vector<Pitch*> _pitchShifters;

    arma::cx_fmat _fft;

    float _fftSize;
    float _fftSizeOut;
    float _stepSize;
    float _stepSizeOut;

    float _channels;

    float _rate;
    float _pitch;
};

#endif // PITCHSHIFT_H
