#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>
#include "gstpitchshift.h"
#include "gstconstq.h"

static gboolean
pitchshift_init (GstPlugin * plugin)
{
    return (gst_element_register(plugin, "pitch", GST_RANK_NONE, GST_TYPE_PITCHSHIFT) &&
	    gst_element_register(plugin, "constq", GST_RANK_NONE, GST_TYPE_CONSTQ));
}

GST_PLUGIN_DEFINE(
        GST_VERSION_MAJOR,
        GST_VERSION_MINOR,
        pitchshift,
        "FFT stuff",
        pitchshift_init,
        VERSION,
        "LGPL",
        "GStreamer",
        "http://www.snakeloops.com/"
        )
