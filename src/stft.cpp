#include "stft.h"
#include <math.h>



//Short Time Fourier Transform
// needed to generate spectrograms
STFT::STFT()
{

#ifdef USE_FFTW
    fftwf_plan_with_nthreads(3);

    _fftw = NULL;
    _ifftw = NULL;
#else
    _fft = NULL;
    _ifft = NULL;
#endif
}

void STFT::init(int fftSize, int stepSize, WinType type)
{
    _fftSize = (fftSize);
    _stepSize = stepSize;

    _win.set_size(_fftSize);

    if  (type == Blackman)
    {
        for(int k=0; k < _fftSize; k++)
            _win[k] =  0.35875 - 0.48829*cos(2*M_PI*k/(_fftSize)) + 0.14128*cos(4*M_PI*k*(_fftSize)) - 0.01168*cos(6*M_PI*k*(_fftSize));
    }
    else if (type == Hann)
    {
        for(int k=0; k < _fftSize; k++)
            _win[k]     = -.5*cos(2.*M_PI*(double)k/((double)_fftSize -1))+.5;//Hann
    }
    else if (type == Hamming)
    {
        for(int k=0; k < (_fftSize); k++)
            _win[k] = (0.53836 - 0.46164*cos( 2.0* M_PI * (k) /((double)_fftSize)));
    }
    _win[_fftSize-1] = 0;

    _storeFwd.set_size((_stepSize<_fftSize? _fftSize-_stepSize: 0));
    _storeFwd.zeros();

    _frameCount = 0;

#ifdef USE_FFTW
    if (_fftw != NULL)
        fftwf_destroy_plan(_fftw);
    _fftw = fftwf_plan_dft_r2c_1d( _fftSize, NULL , NULL, FFTW_ESTIMATE );
#else
    if (_fft != NULL)
        gst_fft_f32_free(_fft);
    _fft = gst_fft_f32_new(_fftSize, false);
#endif
}

STFT::~STFT()
{

}

void STFT::setup (int fftSizeOut, int stepSizeOut, WinType type)
{
    _stepSizeOut = stepSizeOut;
    if (fftSizeOut != _fftSizeOut)
    {
        _fftSizeOut = fftSizeOut;
        _storeInv.resize((_stepSizeOut<_fftSizeOut? _fftSizeOut-_stepSizeOut: 0));
    }


    _winOut.set_size(_fftSizeOut);
    if  (type == Blackman)
    {
        for(int k=0; k < _fftSizeOut; k++)
            _winOut[k] =  0.35875 - 0.48829*cos(2*M_PI*k/(_fftSizeOut)) + 0.14128*cos(4*M_PI*k*(_fftSizeOut)) - 0.01168*cos(6*M_PI*k*(_fftSizeOut));
    }
    else if (type == Hann)
    {
        for(int k=0; k < _fftSizeOut; k++)
            _winOut[k]     = -.5*cos(2.*M_PI*(double)k/((double)_fftSizeOut -1))+.5;//Hann
    }
    else if (type == Hamming)
    {
        for(int k=0; k < (_fftSizeOut); k++)
            _winOut[k] = (0.53836 - 0.46164*cos( 2.0* M_PI * (k) /(_fftSizeOut)));
    }
    _winOut[_fftSizeOut-1] = 0;


#ifdef USE_FFTW
    if (_ifftw != NULL)
        fftwf_destroy_plan(_ifftw);
    _ifftw = fftwf_plan_dft_c2r_1d(_fftSizeOut, NULL, NULL, FFTW_ESTIMATE );
#else
    if (_ifft != NULL)
        gst_fft_f32_free(_ifft);
    _ifft = gst_fft_f32_new(_fftSizeOut, true);
#endif

    //clear current buffers
    _storeFwd.zeros();
    _storeInv.zeros();
}

arma::cx_fmat STFT::forward(const arma::fvec &input)
{
    int frameCount = (input.n_elem /_stepSize);

    if (_frameCount != frameCount)
    {
        _frameCount = frameCount;
    }
    arma::cx_fmat out(_fftSize/2+1, _frameCount);// = _out.set_size(_fftSize/2+1, _frameCount);

    int storeInSize = _storeFwd.n_elem;

    if (storeInSize > 0)// fft's are overlapping (for audio, ie FFT_OSAMP=6), so we must store some data between calls to this func
    {
        arma::fvec inBuf(input.n_elem + storeInSize); // all the samples plus for the next time
        inBuf.subvec(0, storeInSize-1) = _storeFwd;// restore eelmisest korrast
        inBuf.subvec(storeInSize, inBuf.n_elem-1) =  input;// ylej22nud nullida

        arma::fvec tmp(_fftSize);
        for (int i=0; i<frameCount; i++)
        {
            int f = _stepSize * i;
            int t = f+_fftSize-1;

            tmp = inBuf.subvec(f, t) % _win;

#ifdef USE_FFTW
            fftwf_execute_dft_r2c( _fftw,  tmp.memptr() , (fftwf_complex *)out.colptr(i));
#else
            gst_fft_f32_fft(_fft, (const float *)tmp.memptr() , (GstFFTF32Complex *)out.colptr(i));
#endif
        }

        _storeFwd = inBuf.subvec( inBuf.n_elem - storeInSize,   inBuf.n_elem-1);
    }
    else // no fft overlaps (for spectrogram, ie FFT_OSAMP=0.3)
    {
        arma::fvec tmp(_fftSize);
        for (int i=0; i<frameCount; i++)
        {
            int f = _stepSize * i;
            int t = f +_fftSize-1;

            tmp = input.subvec(f, t) % _win;

#ifdef USE_FFTW
            fftwf_execute_dft_r2c( _fftw,  tmp.memptr() , (fftwf_complex *)out.colptr(i));
#else
            gst_fft_f32_fft(_fft, (const float *)tmp.memptr() , (GstFFTF32Complex *)out.colptr(i));
#endif
        }
    }
    return out;
}

// siin inverse ajal peaks genereerima datat juurde
arma::fvec STFT::inverse(const arma::cx_fmat &cx)
{
    int frameCount = cx.n_cols;
    float storeOutSize = _storeInv.n_elem;
    arma::fvec tmpBuf(frameCount*_stepSizeOut + storeOutSize); // all the samples plus for the next time

    //init
    tmpBuf.subvec(0, storeOutSize-1) = _storeInv;// restore eelmisest korrast
    tmpBuf.subvec(storeOutSize, tmpBuf.n_elem - 1).zeros();// ylej22nud nullida

    arma::fvec tmpOut(_fftSizeOut); // do ifft to tmp first
    arma::cx_fvec tmpIn(_fftSizeOut/2+1); // do ifft to tmp first
    for (int i=0; i<frameCount; i++)
    {
        // siin tuleb nyyd scaleda:
        tmpIn.zeros();
        float rate = _stepSizeOut/_stepSize;
        for (int j=0;j<cx.n_rows;j++)
            tmpIn[(int)round(j * rate)] = cx(j, i);// optimize somehow

#ifdef USE_FFTW
        fftwf_execute_dft_c2r(_ifftw, (fftwf_complex*)tmpIn.memptr(), tmpOut.memptr() );
#else
        gst_fft_f32_inverse_fft(_ifft,  (const GstFFTF32Complex *)tmpIn.memptr(), (float *)tmpOut.memptr());
#endif

        // fftSize + frameCount*_stepSize
        int f = i * _stepSizeOut; // first _fftSizeBytes are zero or from last invocation of this func
        int t = f + _fftSizeOut-1;

        tmpBuf.subvec(f, t) += (_winOut % tmpOut);
    }

    // salvesta see j2rgmiseks korraks, returni ylej22nud
    _storeInv = tmpBuf.subvec( tmpBuf.n_elem - storeOutSize,    tmpBuf.n_elem-1);

    return tmpBuf.subvec(0, tmpBuf.n_elem - storeOutSize-1);
}

int STFT::stepSize() const
{
    return _stepSize;
}

arma::fvec STFT::binFreqs(int sampleRate) const
{
    arma::fvec ret(_fftSize/2);

    for (int i=0;i<_fftSize/2;i++)
        ret[i] = i * sampleRate / _fftSize;

    return ret;
}
