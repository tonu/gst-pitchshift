#ifndef STFT_H
#define STFT_H

#include <armadillo>

//#define USE_FFTW

#ifdef USE_FFTW
#include <fftw3.h>
#else
#include <gst/fft/fft.h>
#endif

class STFT
{
public:
    typedef enum __WinType {
        Hann,
        Hamming,
        Blackman
    } WinType;

    STFT();
    ~STFT();

    void init(int fftSize, int stepSize, WinType type = Blackman);
    void setup (int fftSizeOut, int stepSizeOut, WinType type = Blackman);

//    void multi_res_fft(const gfloat *timedata, GstFFTF32Complex *freqdata);

    // framecount must be divideable by 4 threads.
    // output is complex data. user must release.
    // forward on const! inverse jaoks on fifot vaja kuna inverse mixib kokku window abil mitu signaali
    arma::cx_fmat forward(const arma::fvec &input);
    arma::fmat & forward_cqt(const arma::fvec &input);


    // stores last fft size of data in buffer
    arma::fvec inverse(const arma::cx_fmat &cx);

    // first one is zero freq
    // at exactly FS/2 freq - nyquist freq
    arma::fvec binFreqs(int sampleRate) const;

    int stepSize() const;

private:
    float goertzel_mag(int ind, float numSamples,int TARGET_FREQUENCY,int SAMPLING_RATE, const float* data);
    void doGroezl(const float * in, float *out);

    float _fftSize;
    float _fftSizeOut;
    float _stepSize;
    float _stepSizeOut;

    arma::fvec _win;
    arma::fvec _winOut;
    arma::fvec _storeFwd;
    arma::fvec _storeInv;

    arma::cx_fmat _out;
    arma::fmat _outMag;

    float _frameCount;


#ifdef USE_FFTW
    fftwf_plan _fftw;
    fftwf_plan _ifftw;
#else
    GstFFTF32 *_fft;
    GstFFTF32 *_ifft;
#endif


    arma::fvec _cqt_freqs;
    arma::fvec _cqt_period_samples;
    arma::fvec _cqt_cos;
    arma::fvec _cqt_sin;


    arma::fvec _cqt_win[300];//fixme
};

#endif // STFT_H
